# MSE1701


## Office hour

Every week Friday 4:00 pm - 5:30 pm


## TA office hour

Every week Mon 7:00 pm - 8:00 pm

## Evaluation

50% Homework + 25% final project + 25% one quiz (30 min, around week 14-15)


## Homework

Please prepare your results in a form of **report**. The document should be self-explained (all the symbols, figures, and tables need explanation in the main text).

Your code (including post-processing scripts), inputs (parameter file), and some of the outputs when requested are required to submit to Blackboard before due time. 

**Note**, if the size of your output is more than 10 MB, please do **NOT** upload it to the Blackboard.


## Quick usage of git for this course

### git clone

Clone this repository into your local account in HPC (SHT-HPC) (or your local computer if git is installed).

```bash
git clone https://gitlab.com/zhengtheorygroup/mse1701-2024-spring.git
```

Now, you have a local copy of all the input files you need for this practice. 

### git update

If the repository has new information and you want to update your local copy:

```bash
git pull origin main
```

Now, your local copy of the repository is also updated.

:warning: please do not perform calculation inside the folder where you clone the repository, otherwise `git pull` will complain that some files are modified.

## ShanghaiTech HPC (SHT-HPC)

### login

You may login from [here](https://hpc.shanghaitech.edu.cn).

### Use python on SHT-HPC

Use ```$ module load apps/anaconda3/2021.05``` which includes numpy, scipy, and matplotlib.

`apps/anaconda3/2021.05` also includes **numba** lib, but `mpi4py` is not included in this module.

### Job script

STH-HPC uses PBS to organize job submitting. For MSE1701, we use queue name "pub_jx"

**PYTHON** script: If your job is a regular python job, use the following job script:

```bash
#!/bin/bash
#PBS  -N   test                   # job name
#PBS  -l   nodes=1:ppn=1          # serial job (use only 1 node and 1 process), this is enough for a python script
#PBS  -l   walltime=0:30:0        # set up a walltime (usually a short walltime means fast queue time)
#PBS  -S   /bin/bash              # shell envrionment for the job
#PBS  -j   oe                     # save output and error to files
#PBS  -q   pub_jx                 # queue name

cd $PBS_O_WORKDIR

NPROC=`wc -l < $PBS_NODEFILE`

echo This job has allocated $NPROC proc > log

# load python module
module load apps/anaconda3/2021.05


python3 your_script_name.py 
```


**LAMMPS** script: Here is an example of parallel LAMMPS job script:

```bash
#!/bin/bash
#PBS  -N   test                   # job name
#PBS  -l   nodes=1:ppn=4          # resource request: here 1 node and each node with 4 cores are requested (total 4 cores)
#PBS  -l   walltime=0:30:0        # set up a walltime (usually a short walltime means fast queue time)
#PBS  -S   /bin/bash
#PBS  -j   oe 
#PBS  -q   pub_jx                 

cd $PBS_O_WORKDIR

NPROC=`wc -l < $PBS_NODEFILE`

echo This job has allocated $NPROC proc > log

# modules for Lammps
module load apps/lammps/intelmpi/29Oct20

mpirun  --bind-to core -np $NPROC -hostfile $PBS_NODEFILE   lmp_intelmpi -in input >& out
```

